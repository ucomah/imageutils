import UIKit

public struct ImageRotationOptions: OptionSet {
    public let rawValue: UInt
    
    public static let flipOnVerticalAxis = ImageRotationOptions(rawValue: 1 << 0)
    public static let flipOnHorizontalAxis = ImageRotationOptions(rawValue: 1 << 1)
    
    public init(rawValue: UInt) {
        self.rawValue = rawValue
    }
}

public extension UIImage {
    
    @available(iOS 10.0, *)
    func rotated(by rotationAngle: Measurement<UnitAngle>, options: ImageRotationOptions = []) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
        let transform = CGAffineTransform(rotationAngle: rotationInRadians)
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { renderContext in
            renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
            renderContext.cgContext.rotate(by: rotationInRadians)
            
            let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
            let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
            renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))
            
            let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
            renderContext.cgContext.draw(cgImage, in: drawRect)
        }
    }
    
    func rotate(byDegrees degrees: CGFloat) -> UIImage {
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView.init(frame: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let transform = CGAffineTransform(rotationAngle: degrees.toRadians)
        rotatedViewBox.transform = transform
        let rotatedSize = rotatedViewBox.frame.size
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize.multiplied(by: self.scale))
        guard let bitmap = UIGraphicsGetCurrentContext() else {
            return self
        }
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width/2, y: rotatedSize.height/2);
        // Rotate the image context
        bitmap.rotate(by: degrees.toRadians)
        // Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        guard let cg_image = self.cgImage else {
            return self
        }
        bitmap.draw(cg_image, in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))
        // Extract image
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? self
    }
    
    func scale(to i_width: CGFloat) -> UIImage? {
        let oldWidth = self.size.width
        let scaleFactor = i_width / oldWidth
        let newHeight = self.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        return resize(to: CGSize(width: newWidth, height: newHeight))
    }
    
    func resize(to targetSize: CGSize) -> UIImage? {
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        var newSize: CGSize
        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func scale(by scale: CGFloat) -> UIImage? {
        let scaledSize = CGSize(width: size.width * scale, height: size.height * scale)
        return resize(to: scaledSize)
    }
    
    func addTint(withColor color: UIColor) -> UIImage {
        var newImage = self.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(self.size, false, newImage.scale);
        color.set()
        newImage.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        newImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func circular(_ diameter: CGFloat = -1, highlightedColor: UIColor? = nil) -> UIImage? {
        let dia = diameter < 0 ? self.size.width : diameter
        let frame = CGRect(x: 0.0, y: 0.0, width: dia, height: dia)
        UIGraphicsBeginImageContextWithOptions(frame.size, false, self.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        let imgPath = UIBezierPath(ovalIn: frame)
        imgPath.addClip()
        self.draw(in: frame)
        if let hc = highlightedColor {
            context.setFillColor(hc.cgColor)
            context.fillEllipse(in: frame)
        }
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func fixedOrientation(force: Bool = false) -> UIImage? {
        
        if !force && imageOrientation == UIImage.Orientation.up {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

extension UIImage {
    public enum ContentMode {
        case contentFill
        case contentAspectFill
        case contentAspectFit
    }
    
    public func resize(newSize size: CGSize, contentMode: ContentMode = .contentAspectFill) -> UIImage? {
        let aspectWidth = size.width / self.size.width
        let aspectHeight = size.height / self.size.height
        
        switch contentMode {
        case .contentFill:
            return resize(withSize: size)
        case .contentAspectFit:
            let aspectRatio = min(aspectWidth, aspectHeight)
            return resize(withSize: CGSize(width: self.size.width * aspectRatio, height: self.size.height * aspectRatio))
        case .contentAspectFill:
            let aspectRatio = max(aspectWidth, aspectHeight)
            return resize(withSize: CGSize(width: self.size.width * aspectRatio, height: self.size.height * aspectRatio))
        }
    }
    
    private func resize(withSize size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}


public extension UIImage {
    
    func alpha(color: UInt32) -> UInt8 {
        return UInt8((color >> 24) & 255)
    }
    
    func red(color: UInt32) -> UInt8 {
        return UInt8((color >> 16) & 255)
    }
    
    func green(color: UInt32) -> UInt8 {
        return UInt8((color >> 8) & 255)
    }
    
    func blue(color: UInt32) -> UInt8 {
        return UInt8((color >> 0) & 255)
    }
    
    func rgba(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) -> UInt32 {
        let a = UInt32(alpha) << 24
        let r = UInt32(red) << 16
        let g = UInt32(green) << 8
        let b = UInt32(blue) << 0
        return a | r | g | b
    }
    
    func trimTransparentPixels() -> UIImage {
        if (self.size.height < 2 || self.size.width < 2) {
            return self
        }
        guard let inputCGImage = self.cgImage else { return self }
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let width = inputCGImage.width
        let height = inputCGImage.height
        let bytesPerPixel = 4
        let bitsPerComponent = 8
        let bytesPerRow = bytesPerPixel * width
        let bitmapInfo = CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
        
        guard let context = CGContext(data: nil, width: width, height: height,
                                      bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow,
                                      space: colorSpace, bitmapInfo: bitmapInfo)
            else { return self }
        context.draw(inputCGImage, in: CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height)))
        
        guard let pixelData = context.data else {
            return self
        }
        
        let pixelBuffer = pixelData.assumingMemoryBound(to: UInt32.self)
        
        defer {
            //Free memory
            pixelBuffer.deinitialize(count: 1)
        }
        
        var maxRowPixelNumber = -1
        var maxColPixelNumber = -1
        var minRowPixelNumber = Int.max
        var minColPixelNumber = Int.max
        
        // Enumerate through all pixels
        var idx = 0
        for row in 0..<height {
            for col in 0..<width {
                let pixel = pixelBuffer[idx]
                if alpha(color: pixel) != 0 { //Transparent pixel
                    //pixel = rgba(red: 255, green: 0, blue: 0, alpha: 255)
                    if minRowPixelNumber > row {
                        minRowPixelNumber = row
                    }
                    if minColPixelNumber > col {
                        minColPixelNumber = col
                    }
                    if maxRowPixelNumber < row  {
                        maxRowPixelNumber = row
                    }
                    if maxColPixelNumber < col {
                        maxColPixelNumber = col
                    }
                }
                idx += 1
            }
        }
        
        var cropInsets = UIEdgeInsets.zero
        var rect = CGRect(x: 0, y: 0, width: self.size.width * self.scale, height: self.size.height * self.scale)
        
        cropInsets = UIEdgeInsets.init(top: CGFloat(minRowPixelNumber), left: CGFloat(minColPixelNumber),
                                      bottom: rect.size.height - CGFloat(maxRowPixelNumber), right: rect.size.width - CGFloat(maxColPixelNumber))
        
        if (cropInsets.top <= 0 && cropInsets.bottom <= 0 && cropInsets.left <= 0 && cropInsets.right <= 0) {
            // No cropping needed
            return self
        }
        
        // Calculate new crop bounds
        rect.origin.x += cropInsets.left
        rect.origin.y += cropInsets.top
        rect.size.width -= cropInsets.left + cropInsets.right
        rect.size.height -= cropInsets.top + cropInsets.bottom
        
        // Crop it
        guard let newImage = inputCGImage.cropping(to: rect) else {
            return self
        }
        
        // Convert back to UIImage
        let img = UIImage(cgImage: newImage, scale: self.scale, orientation: self.imageOrientation)
        return img
    }
}


public extension UIImage {
    
    func darken() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        defer {
            UIGraphicsEndImageContext()
        }
        guard let context = UIGraphicsGetCurrentContext() else {
            return self
        }
        
        let area = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        
        context.scaleBy(x: 1, y: -1)
        context.translateBy(x: 0, y: -area.size.height)
        context.saveGState()
        guard let img1 = self.cgImage else {
            return self
        }
        context.clip(to: area, mask: img1)
        
        UIColor.darkGray.set()
        context.fill(area)
        
        context.restoreGState()
        
        context.setBlendMode(CGBlendMode.multiply)
        guard let img2 = self.cgImage else {
            return self
        }
        context.draw(img2, in: area)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        return newImage ?? self
    }
}

public extension CALayer {
    
    func renderAsImage(ofSize size: CGSize, rotateAt angle: CGFloat? = nil) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, self.isOpaque, UIScreen.main.scale)
        if let ctx = UIGraphicsGetCurrentContext() {
            //Apply transfomation if present
            let point = self.anchorPoint
            let at = self.affineTransform()
            ctx.translateBy(x: point.x, y: point.y)
            ctx.concatenate(at)
            /*
            if at.b != 0 && at.c != 0 { //Rotation included?
                let b_at = (abs(at.b) < 0.5 ? at.b * size.width : at.b)
                let c_at = abs(at.c) < 0.5 ? at.c * at.c * size.height : at.c * (bounds.height / size.height)
                CGContextTranslateCTM(ctx, b_at, c_at)
                /*
                CGContextTranslateCTM(ctx, at.b, at.c) //bigger
                CGContextTranslateCTM(ctx, at.b * size.width, at.tx * at.c * size.height) //smaller
                */
            } */
            if let degrees = angle {
                var pp = point
                pp.x *= -1
                pp.y *= -1
                pp.x -= at.tx * at.a / 2
                pp.y -= at.ty * at.d / 2
                pp.x /= at.a
                pp.y /= at.d
                ctx.translateBy(x: floor(pp.x + size.width / 2), y: floor(pp.y + size.height / 2))
                ctx.rotate(by: degrees.toRadians)
                ctx.translateBy(x: -floor(pp.x + size.width / 2), y: -floor(pp.y + size.height / 2))
            }
            ctx.translateBy(x: (-point.x - at.tx) / at.a, y: (-point.y - at.ty) / at.d)
            //Render & export
            self.render(in: ctx)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return img
        }
        return nil
    }
    
    var renderedAsImage: UIImage? {
        return self.renderAsImage(ofSize: self.frame.size)
    }
    
    func transformByRotatingAroundCenter(byDegrees degrees: CGFloat) {
        /**
         `center` is the center of your layer,
         `rotationAngle` is in radians (positive is counterclockwise),
         `rotationPoint` is the point about which you wish to rotate.
         `center` and `rotationPoint` are in the coordinate space of the containing view.
         */
        let rotationAngle = degrees.toRadians
        let center = self.frame.center
        let rotationPoint = CGPoint(x: self.frame.width, y: self.frame.height)
        var transform = self.transform //CATransform3DIdentity
        //FIXME: Needs refactoring
        if self.anchorPoint == CGPoint.zero {
            transform = CATransform3DTranslate(transform, rotationPoint.x - center.x, rotationPoint.y - center.y, 0.0)
        }
        transform = CATransform3DRotate(transform, rotationAngle, 0.0, 0.0, 1.0)
        if self.anchorPoint == CGPoint.zero {
            transform = CATransform3DTranslate(transform, center.x - rotationPoint.x, center.y - rotationPoint.y, 0.0)
        }
        self.transform = transform
    }
    
    func transformByScalingWith(_ sx: CGFloat, sy: CGFloat, sz: CGFloat) {
        let center = self.frame.center
        let rotationPoint = CGPoint(x: self.frame.width, y: self.frame.height)
        var transform = self.transform
        transform = CATransform3DTranslate(transform, rotationPoint.x - center.x, rotationPoint.y - center.y, 0.0)
        transform = CATransform3DScale(transform, sx, sy, sz)
        transform = CATransform3DTranslate(transform, center.x - rotationPoint.x, center.y - rotationPoint.y, 0.0)
        self.transform = transform
    }
    
    @discardableResult
    func scale(toWidth width: CGFloat) -> CALayer {
        let divisor = width / self.frame.width
        let multiplier: CGFloat = divisor < 1 ? -1.0 : 1.0
        let ap = CGPoint(x: self.frame.center.x * multiplier, y: self.frame.center.x * multiplier)
        self.transformByScalingWith(divisor, sy: divisor, sz: 1.0)
        self.anchorPoint = ap
        return self
    }
    
    var borderUIColor: UIColor? {
        set {
            self.borderColor = newValue?.cgColor
        }
        get {
            if let bc = borderColor {
                return UIColor(cgColor: bc)
            }
            else {
                return nil
            }
        }
    }
}

public extension CGRect {
    var center: CGPoint {
        return CGPoint(x: self.width / 2, y: self.height / 2)
    }
}

extension CGSize {
    /** Adjusts current height & width to fit provided size values as a maximal values
     */
    public func sizeAdjustedToFit(size preferredSize: CGSize) -> CGSize {
        let maxW = preferredSize.width
        let maxH = preferredSize.height
        let width = self.width
        let height = self.height
        var result: CGSize
        if width < height { //Is portrait image size?
            result = CGSize(width: maxW * width / height, height: maxH)
        } else {
            result = CGSize(width: maxW, height: maxH * height / width)
        }
        return result
    }
}

public extension CGFloat {
    var toRadians: CGFloat {
        return self * CGFloat.pi / 180
    }
}

fileprivate extension CGSize {
    
    func multiplied(by value: CGFloat) -> CGSize {
        return CGSize(width: self.width * value, height: self.height * value)
    }
}

public extension UIImage {
    
    static var isHeicSupported: Bool {
        (CGImageDestinationCopyTypeIdentifiers() as! [String]).contains("public.heic")
    }
    
    var heic: Data? { heic() }
    
    func heic(compressionQuality: CGFloat = 1) -> Data? {
        guard Self.isHeicSupported else {
            return nil
        }
        guard
            let mutableData = CFDataCreateMutable(nil, 0),
            let destination = CGImageDestinationCreateWithData(mutableData, "public.heic" as CFString, 1, nil),
            let cgImage = cgImage
        else { return nil }
        CGImageDestinationAddImage(destination, cgImage, [kCGImageDestinationLossyCompressionQuality: compressionQuality, kCGImagePropertyOrientation: cgImageOrientation.rawValue] as CFDictionary)
        guard CGImageDestinationFinalize(destination) else { return nil }
        return mutableData as Data
    }
}

extension CGImagePropertyOrientation {
    init(_ uiOrientation: UIImage.Orientation) {
        switch uiOrientation {
            case .up: self = .up
            case .upMirrored: self = .upMirrored
            case .down: self = .down
            case .downMirrored: self = .downMirrored
            case .left: self = .left
            case .leftMirrored: self = .leftMirrored
            case .right: self = .right
            case .rightMirrored: self = .rightMirrored
        @unknown default:
            fatalError()
        }
    }
}

extension UIImage {
    var cgImageOrientation: CGImagePropertyOrientation { .init(imageOrientation) }
}
