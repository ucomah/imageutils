// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "ImageUtils",
    platforms: [
      .iOS(.v9)
    ],
    products: [        
        .library(
            name: "ImageUtils",
            targets: ["ImageUtils"]
        )
    ],
    targets: [     
        .target(
            name: "ImageUtils",
            path: "ImageUtils"
        )
    ]
)
